﻿using System;
using System.ComponentModel;
using System.Linq;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using XFArchitecture.iOS.Effects;

[assembly: ResolutionGroupName("ITexico")]
[assembly: ExportEffect(typeof(BorderViewEffect), nameof(BorderViewEffect))]
namespace XFArchitecture.iOS.Effects
{
    public class BorderViewEffect : PlatformEffect
    {
        public BorderViewEffect()
        {
        }

        protected override void OnAttached()
        {
            try
            {
                var effect = (XFArchitecture.Effects.BorderViewEffect)Element.Effects.FirstOrDefault(e => e is XFArchitecture.Effects.BorderViewEffect);
                var view = Control != null ? Control : Container;
                if (effect != null && view != null)
                {
                    view.Layer.CornerRadius = effect.Radius;
                    view.Layer.BorderColor = effect.BorderColor.ToCGColor();
                    view.Layer.BorderWidth = effect.BorderWidth;
                    if (effect.HasShadow)
                    {
                        view.Layer.ShadowColor = UIColor.LightGray.CGColor;
                        view.Layer.ShadowRadius = 5;
                        view.Layer.ShadowOpacity = .7f;
                        view.Layer.ShadowOffset = new CGSize(0, 5f);
                        view.Layer.MasksToBounds = false;
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot set property on attached control. Error: " + ex.Message);
            }
        }

        protected override void OnDetached()
        {
            throw new NotImplementedException();
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);
        }
    }
}
