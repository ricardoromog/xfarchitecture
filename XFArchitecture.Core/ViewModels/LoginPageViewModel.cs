﻿using System;
using System.Windows.Input;
using System.ComponentModel;

using XFArchitecture.Core.Helpers;
using XFArchitecture.Core.Contracts;

namespace XFArchitecture.Core.ViewModels
{
    public class LoginPageViewModel : XFBaseViewModel
    {
        private string username;
        public string Username
        {
            get => username;
            set => SetProperty(ref username, value);
        }

        private string password;
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }

        public XFCommand LoginCommand { get; set; }

        private ILoginContract contract;
        public LoginPageViewModel(ILoginContract contract)
        {
            this.contract = contract;
            LoginCommand = new XFCommand(LoginAction);
        }

        private void LoginAction()
        {
            contract.GoToMainScreen();
        }

    }
}
