﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using XFArchitecture.Core.Models;

namespace XFArchitecture.Core.Services
{
	public class ServicesManager : BaseServicesConsumer
    {
		private static Lazy<ServicesManager> lazy = new Lazy<ServicesManager>(() => new ServicesManager());
        public static ServicesManager Instance { get { return lazy.Value; } }

		private string baseCall = "/classes/{0}";
        public ServicesManager()
        {

        }

        public async Task<ToDoEntity> CreateItem(ToDoEntity myObj)
        {
            return await Task.Run(() =>
            {
                var url = string.Format(baseCall, nameof(ToDoEntity));
                return MakePostCall<BaseWrapper<ToDoEntity>>(url, myObj).Data;
            });
        }
        
        public async Task<List<ToDoEntity>> GetItems()
        {
            return await Task.Run(() =>
            {
                var url = string.Format(baseCall,nameof(ToDoEntity));
                return MakeGetCall<BaseWrapper<List<ToDoEntity>>>(url).Data;
            });
        }

        public async Task<string> UploadImage(string path)
        {
            return await Task.Run(() =>
            {
                return MakeImagePostCall<string>(path);
            });
        }
    }
}
