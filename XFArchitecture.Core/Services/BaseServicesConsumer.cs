﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

using Newtonsoft.Json;

using XFArchitecture.Core.Helpers;

namespace XFArchitecture.Core.Services
{
    public class BaseServicesConsumer
    {
        private HttpClient Client { get; set; }
        private WebRequest WebRequest { get; set; }
        private WebRequest tests;

        public BaseServicesConsumer()
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri(Constants.ParseBaseURL);
            Client.DefaultRequestHeaders.Add("X-Parse-REST-API-Key", Constants.ParseRestID);
            Client.DefaultRequestHeaders.Add("X-Parse-Application-Id", Constants.ParseAppID);

            WebRequest = WebRequest.Create(Constants.ParseBaseURL + "/files/" + "image.jpg");
            WebRequest.Method = "POST";
            WebRequest.Headers.Add("X-Parse-Application-Id", Constants.ParseAppID);
            WebRequest.Headers.Add("X-Parse-REST-API-Key", Constants.ParseRestID);
            WebRequest.ContentType = Constants.ImageContentType;

        }

        protected T MakeGetCall<T>(string urlFormat)
        {
            Func<T> f = () =>
            {
                var response = Client.GetAsync(urlFormat).Result;
                var content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                return content;
            };

            return RunFunction(f);
        }


        protected T MakePostCall<T>(string urlFormat, dynamic myObj)
        {
            Func<T> f = () =>
            {
                string strContent = JsonConvert.SerializeObject(myObj);
                var response = Client.PostAsync(urlFormat,
                                                new StringContent(strContent,
                                                                 Encoding.UTF8,
                                                                  Constants.JsonContentType)).Result;

                var content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                return content;
            };

            return RunFunction(f);
        }

        protected T MakeImagePostCall<T>(string urlFormat, dynamic myObj)
        {
            Func<T> f = () =>
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(myObj.Base64Image);
                string strContent = JsonConvert.SerializeObject(myObj);
                var response = Client.PostAsync(urlFormat,
                                                new StringContent(strContent,
                                                                 Encoding.UTF8,
                                                                  Constants.ImageContentType)).Result;

                dynamic content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                return content;
            };

            return RunFunction(f);
        }

        protected T MakePutCall<T>(string urlFormat, dynamic myObj)
        {
            Func<T> f = () =>
            {
                string strContent = JsonConvert.SerializeObject(myObj);
                System.Diagnostics.Debug.WriteLine(strContent);
                var response = Client.PutAsync(urlFormat,
                                                new StringContent(strContent, Encoding.UTF8,
                                                                  Constants.JsonContentType)).Result;

                dynamic content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                return content;

            };

            return RunFunction(f);
        }

        protected T MakeDeleteCall<T>(string urlFormat)
        {
            Func<T> f = () =>
            {
                var response = Client.DeleteAsync(urlFormat).Result;

                dynamic content = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                return content;
            };

            return RunFunction(f);
        }

        protected T MakeImagePostCall<T>(string filepath)
        {
            Func<T> f = () =>
            {
                using (var s = new FileStream(filepath, FileMode.Open))
                {
                    dynamic content = UploadImage(s, Path.GetFileName(filepath));
                    return content;
                }
            };
            return RunFunction(f);
        }

        private string UploadImage(Stream source, string filpath)
        {
            using (var sreq = WebRequest.GetRequestStream())
            {
                var buffer = new byte[4 * 2048]; int n;
                while ((n = source.Read(buffer, 0, buffer.Length)) > 0)
                    sreq.Write(buffer, 0, n);
            }

            using (var sr = new StreamReader(WebRequest.GetResponse().GetResponseStream()))
            {
                var jsonresponse = sr.ReadToEnd();
                dynamic content = JsonConvert.DeserializeObject<dynamic>(jsonresponse);
                return content["url"];
            }
        }


        private T RunFunction<T>(Func<T> func)
        {
            return func();
        }
    }
}
