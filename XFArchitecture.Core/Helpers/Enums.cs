﻿using System;

namespace XFArchitecture.Core.Helpers
{
   
    public enum Status
    {
        Done = 0,
        InProgress = 1,
		Pending = 2
    }   

	public enum PageType
	{
        TasksListPage,
        TaskDetailsPage,
        AboutPage
	}
}
