﻿using System;
using System.Windows.Input;

namespace XFArchitecture.Core.Helpers
{
    public class XFCommand : ICommand
    {
        private readonly Action _handler;

        public XFCommand(Action handler)
        {
            _handler = handler;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _handler != null;
        }

        public void Execute(object parameter)
        {
            _handler();
        }
    }
}
