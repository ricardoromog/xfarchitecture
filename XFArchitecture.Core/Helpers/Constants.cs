﻿using System;
namespace XFArchitecture.Core.Helpers
{
	public class Constants
	{
		public const string ParseBaseURL = "https://parseapi.back4app.com/";
		public const string ParseAppID = "Rrf37AZkWSYrzxcNH76W8KyNhASMjVqmTxAUU9fo";
		public const string ParseRestID = "kbGJeO8VZobLnI25iRwA33hKTRm9l9wruKOGAU1Y";
		public const string JsonContentType = "application/json";
        public const string ImageContentType = "image/jpeg";
	}
}
