﻿using System;
using MvvmHelpers;
using XFArchitecture.Core.Helpers;

namespace XFArchitecture.Core.Models
{
    public class ToDoEntity : ObservableObject
    {
		public string Id { get; set; }
		public string Title { get; set; }
		public string Image { get; set; }
		public string Description { get; set; }
		public Status Status { get; set; }
		public DateTime Date { get; set; }
    }
}
