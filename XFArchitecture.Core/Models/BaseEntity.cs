﻿using System;

using Newtonsoft.Json;

namespace XFArchitecture.Core.Models
{
    public class BaseEntity
    {
        [JsonProperty("objectId")]
        public string ObjectId { get; set; }
        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }

    public class BaseWrapper<T> : BaseEntity
    {
        [JsonProperty("results")]
        public T Data { get; set; }
    }
}
