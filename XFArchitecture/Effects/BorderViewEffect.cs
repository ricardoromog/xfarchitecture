﻿using System;
using Xamarin.Forms;

namespace XFArchitecture.Effects
{
    public class BorderViewEffect : RoutingEffect
    {

        public float Radius { get; set; }

        public float BorderWidth { get; set; }
        public bool HasShadow { get; set; }
        public Color BorderColor { get; set; };

        public static readonly BindableProperty RadiusProperty =
            BindableProperty.Create(nameof(Radius), typeof(string), typeof(BorderViewEffect), 1);

        public BorderViewEffect() : base("BroncosFC.BorderedViewEffect")
        {
            
        }
    }
}
