﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;    

using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using XFArchitecture.Helpers;
using XFArchitecture.Core.Models;
using XFArchitecture.Core.Services;

namespace XFArchitecture
{
    public partial class MainPage : ContentPage
    {
		public MainPage()
        {
            InitializeComponent();
        }

		private async void Handle_Clicked(object sender, System.EventArgs e)
		{
			await CrossMedia.Current.Initialize();

			var media = CrossMedia.Current;
            var file = await media.PickPhotoAsync();
            var ok = await ServicesManager.Instance.UploadImage(file.Path);
			//var test = await HelperMethods.Instance.ConvertImageToBase64(file);

			//var ok = new ToDoEntity() { };
			//ok.Title = "First Image";
			//ok.Description = "Hello";
			//ok.Image = test;
			//ok.Date = new DateTime();
			//ok.Status = Core.Helpers.Status.Pending;

			//var test = ServicesManager.Instance.GetItems();
			//var ok = true;
		}
    }
}
