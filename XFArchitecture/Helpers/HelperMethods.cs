﻿using System;
using System.Threading.Tasks;

using Plugin.Media.Abstractions;

namespace XFArchitecture.Helpers
{
    public class HelperMethods
    {
       
		private static Lazy<HelperMethods> lazy = new Lazy<HelperMethods>(() => new HelperMethods());
		public static HelperMethods Instance { get { return lazy.Value; } }

        public HelperMethods()
        {

        }
    }
}
