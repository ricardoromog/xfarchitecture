﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using XFArchitecture.Core.Contracts;
using XFArchitecture.Core.ViewModels;

namespace XFArchitecture.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPageViewModel ViewModel { get => BindingContext as LoginPageViewModel; }
        public LoginPage()
        {
            BindingContext = new LoginPageViewModel(this);
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            SetProperties();
        }


        private void SetProperties()
        {
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
