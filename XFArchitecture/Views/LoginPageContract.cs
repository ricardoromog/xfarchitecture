﻿using System;
using XFArchitecture.Core.Contracts;

namespace XFArchitecture.Views
{
    public partial class LoginPage : ILoginContract
    {
        public void GoToMainScreen()
        {
            Navigation.PushAsync(new XFAMasterDetailsPage());
        }
    }
}
